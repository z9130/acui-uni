import { defineConfig, loadEnv, ConfigEnv } from 'vite';
import uni from "@dcloudio/vite-plugin-uni";

import { resolve } from 'path';

import Unocss from 'unocss/vite';
// import AutoImport from 'unplugin-auto-import/vite'
import UniPages from '@uni-helper/vite-plugin-uni-pages'
// import UniLayouts from '@uni-helper/vite-plugin-uni-layouts'

// import VueDevTools from 'vite-plugin-vue-devtools'


const pathResolve = (dir: string) => {
  return resolve(__dirname, '.', dir);
};

const alias: Record<string, string> = {
  '/@': pathResolve('./src/'),
};


// https://vitejs.dev/config/
export default defineConfig((mode: ConfigEnv) => {
  const env = loadEnv(mode.mode, process.cwd());

  const VITE_GLOB_API_URL: string = env.VITE_GLOB_API_URL as string;
  const VITE_GLOB_API_URL_PREFIX: string = env.VITE_GLOB_API_URL_PREFIX as string;

  return {
    server: {
      host: '0.0.0.0',
      port: env.VITE_PORT as unknown as number,
      hmr: true,
      proxy: {
        [VITE_GLOB_API_URL_PREFIX]: {
          target: VITE_GLOB_API_URL,
          ws: true,
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, '/'),
        },
      },
    },
    plugins: [
      /**
        * vite-plugin-uni-pages
        * @see https://github.com/uni-helper/vite-plugin-uni-pages
        */
      UniPages({
        subPackages: [
          // 'src/pages-sub',
        ],
      }),
      /**
       * vite-plugin-uni-layouts
       * @see https://github.com/uni-helper/vite-plugin-uni-layouts
       */
      // UniLayouts(),

      /**
       * unplugin-auto-import 按需 import
       * @see https://github.com/antfu/unplugin-auto-import
       */
      // AutoImport({
      //   imports: [
      //     'vue',
      //     'uni-app',
      //   ],
      //   dts: true,
      //   dirs: [
      //     './src/composables',
      //   ],
      //   vueTemplate: true,
      // }),

      uni(),

      /**
       * unocss
       * @see https://github.com/antfu/unocss
       * see unocss.config.ts for config
      */
      Unocss(),

      /**
       * vite-plugin-vue-devtools 增强 Vue 开发者体验
       * @see https://github.com/webfansplz/vite-plugin-vue-devtools
       */
      // VueDevTools(),
    ],
  }
});
