#  介绍

`wot-design-uni`组件库基于`vue3`+`Typescript`构建，参照`wot desing`的设计规范进行开发，旨在给开发者提供统一的UI交互，同时提高研发的开发效率。

## 快速上手

请查看[快速上手](/guide/quickstart.html)文档。

## 扫码体验
