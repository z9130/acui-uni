
import { router as r } from "@/uni_modules/ac-ui/utils/router"


let customRouter = {
    ...r,
    toLogin: () => {
        r.push({ url: '/pages/login/login' })
    },
    toHome: () => {
        r.push({ url: '/pages/service/todo/index', tabBar: true })
        // r.push({ url: '/pages/service/todo/index', tabBar: true })
    }
}


export const router = customRouter
