# 按钮 Button

## 说明

按钮组件。

## 示例效果

<div class="uniapp-demo-box">

<<< ./src/pages/component/button.vue
<webview url="/pages/component/button"></webview>

</div>

## 示例

```vue
<ac-button></ac-button>
```

## 参数 Props

| 属性相关 | 说明 | 类型 |默认值| 可选值 |
| --- | --- | --- | --- | --- |
| text | 按钮文字 | string | - | - |
| type | 类型 | string | 'normal' | primary info success warning danger |
| size | 尺寸 | string | 'normal' | mini small large |
| disabled | 是否禁用 | boolean | false | true |
| loading | 加载状态 | boolean | false | true |
| openType | 开放能力 | string | - | [见官方文档](https://uniapp.dcloud.net.cn/component/button.html) |

| 样式相关 | 说明 | 类型 | 默认值 | 可选值 |
| --- | --- | --- | --- | --- |
| text-size | 按钮大小，rpx | string | 'default' | 'mini','small','default','large' |
| text-color | 文字颜色 | string | 'grey-10' | [主题颜色](https://www.yuque.com/acui/docs/fwshtn) |
| _class | - | string | - | - |
| _style | - | string | - | - |

## 事件 Event

| 事件名 | 描述 | 参数 | 返回值 |
| --- | --- | --- | --- |
| click | - | - | - |
| longpress |  |  |  |
| getphonenumber |  |  |  |
| getuserInfo |  |  |  |
| getUserProfile |  |  |  |
|  |  |  |  |

##  插槽 slot

| 插槽名称 | 描述 |
| -------- | ---- |
| default  | -    |

## 注意事项

无
