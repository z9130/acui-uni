
const sidebar = {
    '/guide': [
        {
            text: '介绍',
            items: [
                { text: 'AC UI', link: '/guide/introduction' },

            ]
        },
        {
            text: '指南',
            items: [
                { text: '快速上手', link: '/guide/quickstart' },
                { text: '定制主题', link: '/guide/customize-theme' },
                { text: '全局配置', link: '/guide/config' },
                { text: '更新日志', link: '/guide/changelog' },
                { text: '常用代码块', link: '/guide/codeblock' },
                { text: '常见问题', link: '/guide/problem' },
            ]
        }
    ],
    '/components': [
        {
            text: '基础组件',
            items: [
                { text: '按钮 Button', link: '/components/button' },
                { text: '全局配置 ConfigProvider', link: '/components/config-provider' },
                { text: '图片 Image', link: '/components/image' },
                { text: '文本 Text', link: '/components/text' },
                { text: '标签 Tag', link: '/components/tag' },
            ]
        },
        // {
        //     text: '布局组件',
        //     items: [
        //         { text: '文本 Text', link: '/components/avatar' },
        //         { text: '按钮 Button', link: '/components/avatar' },
        //         { text: '头像 Avatar', link: '/components/avatar' },
        //         { text: '标签 Tag', link: '/components/avatar' },
        //         { text: 'FAQ', link: '/components/avatar' },
        //     ]
        // },
        {
            text: '数据录入组件',
            items: [
                { text: '复选框 Checkbox', link: '/components/checkbox' },
                { text: '文本输入 Input', link: '/components/input' },
                { text: '数字输入 Input Number', link: '/components/input-number' },
                { text: '单选 Radio', link: '/components/radio' },
                { text: '开关 Switch', link: '/components/switch' },
                { text: '上传 Upload', link: '/components/upload' },
            ]
        },
        {
            text: '数据展示组件',
            items: [
                { text: '头像 Avatar', link: '/components/avatar' },
                { text: '单元格 Cell', link: '/components/cell' },
                { text: '分割线 Divider', link: '/components/divider' },
                { text: '页脚 Footer', link: '/components/footer' },
            ]
        },
        {
            text: '导航组件',
            items: [
                { text: '顶部导航栏 Navbar', link: '/components/navbar' },
                { text: '底部导航栏 Tabbar', link: '/components/tabbar' },
                { text: '标签页 Tabs', link: '/components/tabs' },

            ]
        },
        {
            text: '反馈组件',
            items: [
                { text: '轻提示 Toast', link: '/components/toast' },
                { text: '消息提示 Message', link: '/components/message' },
                { text: '加载 Loading', link: '/components/loading' },
                { text: '旋转动画 Spin', link: '/components/spin' },
                { text: '动画 Lottie', link: '/components/lottie' },
            ]
        },
        // {
        //     text: '其它组件',
        //     items: [

        //     ]
        // },
    ]
}

export default sidebar;