import Theme from "vitepress/theme";
import { h } from "vue";

import AnimateTitle from "./components/AnimateTitle.vue";
import frame from './components/frame.vue'
import webview from './components/mobileWebview.vue'


import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/reset.css';

import './style/scrollbar.css'
import './style/vars.css'
import './style/search.css'


import './custom.css'

export default {
    ...Theme,
    Layout() {
        return h(Theme.Layout, null, {
            // "home-hero-info": () => h(AnimateTitle),
        });
    },
    enhanceApp({ app }) {
        app.use(Antd)
        app.component('frame', frame)
        app.component('webview', webview)
    },
};
