import toast from './utils/function/toast.js';
import navigate from './utils/function/navigate.js';
// import time from './utils/function/time'
import aCache from './utils/cache/index.js'; //uni.v.cache.xxx()
// import pubFunction from '@/common/acui/pubFunction'

import aliyunOSSUtil from './utils/function/aliyunOSSUtil';
import pubfn from './utils/function/index';

function navigateToLogin(data: { mode: string }) {
	// #ifdef MP-WEIXIN
	uni.navigateTo({
		url: '/pages/login/mpweixin',
	});
	// #endif

	// #ifdef H5
	uni.navigateTo({
		url: '/pages/login/mobile',
	});
	// #endif
	return;
}

const ac = {
	aliyunOSSUtil,
	navigateToLogin: navigateToLogin,
	pubfn,
	// myfn: getApp().globalData.myfn,
} as any;

ac.use = function (obj: any, util: any) {
	for (let name in obj) {
		if (obj[name] && typeof obj[name].init === 'function') {
			obj[name].init(util);
		}
		ac[name] = obj[name];
	}
};

uni.$ac = ac;

export default {
	install: (app: any) => {
		ac.use({ cache: aCache });
		ac.use(toast);
		ac.use(navigate);
		app.config.globalProperties.ac = ac;
	},
};
