/**
 * 函数 - 弹窗
 */
export default {
	/**
	 * 轻提示
	 uni.v.toast('暂无数据')
	 */
	showToast: function (title = '', icon = 'none', mask = false) {
		switch (title) {
			case 'fail':
				title = '服务器好像出了点问题，请重试';
				break;
			default:
				break;
		}
		uni.showToast({
			title,
			icon,
			mask,
			duration: 1500,
		});
	},
	/**
	 * 加载提示
	 uni.v.loading('审核中')
	 */
	showLoading: function (title = '加载中', mask = true) {
		uni.showLoading({
			title,
			mask,
		});
	},
	/*
		uni.$ac.showConfirm('这是提示框', '提示').then((res) => {})
	 */
	showConfirm: function (content = '', title = '提示', confirmText = '确认', cancelText = '取消') {
		return new Promise(function (resolve, reject) {
			uni.showModal({
				title,
				content,
				confirmText,
				cancelText,
				showCancel: true,
				confirmColor: '#576B95',
				cancelColor: '#000000',
				success: function (res) {
					resolve(res);
				},
			});
		});
	},

	/*
		uni.$ac.showAlert('这是提示框', '提示').then((res) => {})
	 */
	showAlert: function (content = '', title = '提示', confirmText = '确认') {
		return new Promise(function (resolve, reject) {
			uni.showModal({
				title,
				content,
				confirmText,
				showCancel: false,
				confirmColor: '#576B95',
				cancelColor: '#000000',
				success: function (res) {
					resolve(res);
				},
			});
		});
	},

	/*
		uni.$ac.prompt('请输入', '提示','确认','取消').then((res) => {})
	 */
	showPrompt: function (placeholderText = '请输入', title = '提示', confirmText = '确认', cancelText = '取消') {
		return new Promise(function (resolve, reject) {
			// 当只有一个参数时，默认输出为content
			if (content == '') {
				content = title;
				title = '';
			}
			uni.showModal({
				placeholderText,
				title,
				confirmText,
				cancelText,
				editable: true,
				confirmColor: '#576B95',
				cancelColor: '#000000',
				success: function (res) {
					resolve(res);
				},
			});
		});
	},

	/**
	 * 操作菜单
	 uni.$ac.showActionSheet(['1', '2']).then((res) => {
	 	console.log(res);
	 })
	 */
	showActionSheet: function (obj = { title, itemList, alertText, itemColor, popover }) {
		return new Promise(function (resolve, reject) {
			const { title, alertText, itemList, itemColor = '#000000', popover = {} } = obj;
			uni.showActionSheet({
				title,
				itemList,
				alertText,
				itemColor,
				popover,
				success: function (res) {
					resolve(res.tapIndex);
				},
			});
		});
	},
};
