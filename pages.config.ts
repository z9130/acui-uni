import { defineUniPages } from '@uni-helper/vite-plugin-uni-pages'

export default defineUniPages({
  /**
    * homePage string 设置默认路由入口
    * @default 'pages/index' || 'pages/index/index'
    */
  // homePage: "pages/login/login",
  // homePage: "pages/basic/index",
  homePage: "pages/component/index",


  /**
    * dir string 扫描的目录
    * @default 'src/pages'
    */
  dir: 'src/pages',
  pages: [],
  /**
   * subPackages string[] 扫描的目录，例如：src/pages-sub
   */
  subPackages: [],
  /**
   * exclude string[] 排除的页面 
   * @default []
   */
  exclude: [],
  globalStyle: {
    "navigationBarTextStyle": "black",
    "navigationBarTitleText": "",
    "navigationBarBackgroundColor": "#FFFFFF",
    "backgroundColor": "#FFFFFF"
  },
  "tabBar": {
    "color": "#bfbfbf",
    "selectedColor": "#0165FF",
    "backgroundColor": "#ffffff",
    "list": [{
      "pagePath": "pages/basic/index",
      "iconPath": "static/tabbar/home.png",
      "selectedIconPath": "static/tabbar/home_selected.png",
      "text": "基础"
    },
    {
      "pagePath": "pages/component/index",
      "iconPath": "static/tabbar/component.png",
      "selectedIconPath": "static/tabbar/component_selected.png",
      "text": "组件"
    },
    {
      "pagePath": "pages/template/index",
      "iconPath": "static/tabbar/template.png",
      "selectedIconPath": "static/tabbar/template_selected.png",
      "text": "模板"
    },
    {
      "pagePath": "pages/about/index",
      "iconPath": "static/tabbar/about.png",
      "selectedIconPath": "static/tabbar/about_selected.png",
      "text": "关于"
    }
    ]
  },
})
