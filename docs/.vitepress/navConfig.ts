const nav = [
    // {
    //     text: "主页", link: "/"
    // },
    {
        text: '首页', link: "/"
    },
    {
        text: '文档',
        items: [
            { text: '介绍', link: '/guide/introduction' },
            { text: '快速上手', link: '/guide/quickstart' },
            { text: '定制主题', link: '/guide/customize-theme' },
            { text: '全局配置', link: '/guide/config' },
            { text: '更新日志', link: '/guide/changelog' },
            { text: '常用代码块', link: '/guide/codeblock' },
            { text: '常见问题', link: '/guide/problem' },
        ]
    },
    {
        text: '组件',
        items: [
            { text: '基础组件', link: '/components/button' },
            { text: '数据录入组件', link: '/components/auto-complete' },
            { text: '数据展示组件', link: '/components/avatar' },
            { text: '导航组件', link: '/components/action-sheet' },
            { text: '反馈组件', link: '/components/alert' },
        ]
    },
    {
        text: '工具库',
        items: [
            { text: '请求 Http', link: '/library/http' },
            { text: '路由 Router', link: '/library/router' },
            { text: '分享 Share', link: '/library/share' },
            { text: '常用函数', link: '/library/function' },
        ]
    },
    // { text: '图标', link: '/icon/introduction' },
    // { text: '物料商城', link: '/markdown-examples' },
    {
        text: '相关链接',
        items: [
            { text: 'uniapp 插件市场', link: '/library/share' },
        ]
    },
    // { text: '源码仓库', link: '/markdown-examples' },
];
export default nav;