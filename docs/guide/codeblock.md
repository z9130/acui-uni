# 代码块

````json
``` json
"ac-base": {
  "prefix": "ac-base",
  "body": [
    "<template>",
    "\t<ac-view>",
    "\t\t$0",
    "\t</ac-view>",
    "",
    "\t<ac-message ref=\"msg\"></ac-message>",
    "</template>",
    "",
    "<script setup>",
    "",
    "",
    "",
    "</script>",
    "",
    "<style scoped>",
    "",
    "</style>"
  ],
  "description": "ac-base"
},
"ac-app": {
  "prefix": "ac-app",
  "body": ["<ac-app>$0</ac-app>"],
  "description": "ac-app"
},
"ac-view": {
  "prefix": "ac-view",
  "body": ["<ac-view _class=\"\">$0</ac-view>"],
  "description": "ac-view"
},
"ac-text": {
  "prefix": "ac-text",
  "body": ["<ac-text color=\"\" font-size=\"28\" :label=\"\" _class=\"\">$0"],
  "description": "ac-text"
},
"ac-image": {
  "prefix": "ac-image",
  "body": ["<ac-image src=\"\" _class=\"\">$0</ac-image>"],
  "description": "ac-image"
},
"ac-button": {
  "prefix": "ac-button",
  "body": ["<ac-button type=\"button\">$0</ac-button>"],
  "description": "ac-button"
},
"ac-icon": {
  "prefix": "ac-icon",
  "body": ["<ac-icon name=\"\">$0</ac-icon>"],
  "description": "ac-icon"
},
"ac-message": {
  "prefix": "ac-message",
  "body": ["<ac-message ref=\"msg\" >$0"],
  "description": "ac-message"
}
```
````

