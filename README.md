友情感谢
[Svg Badge Tool](https://gitee.com/hamm/svg_badge_tool)
一个快速生成 SVG 的小工具，支持传入参数进行生成 svg 的可外链小图标。
[shields.io](https://shields.io/category/build) 制作徽标
通过下方链接可快速将指定文件下载到指定位置

```bash
curl -o "F:\image.svg" "https://svg.hamm.cn/gitee.svg?type=star&user=open-source-byte&project=source-vue"
```

网站链接
在线玩转 css 动画、阴影、色彩等：https://keyframes.app
