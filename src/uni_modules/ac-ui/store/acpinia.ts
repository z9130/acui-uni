import { defineStore } from 'pinia';

import { themeArr } from '@/uni_modules/ac-ui/theme/index';

export const useAcPiniaStore = defineStore('theme', {
	state: () => {
		// light  dark
		// return { theme: 'dark', themeArr: themeArr };
		return {
			tabBarIndex: 0,
			tabBarShow: true,
			theme: getApp().globalData.theme || 'light',
			isGrayPage: uni.getStorageSync('ac_isGrayPage') || false,
			themeArr: themeArr,
		};
	},

	// 也可以这样定义
	// state: () => ({ count: 0 })
	actions: {
		getGrayPage() {
			return this.isGrayPage;
		},
		changeGrayPage(isGray = false) {
			this.isGrayPage = isGray;
			uni.setStorageSync('ac_isGrayPage', isGray);
		},
		changeTabBarIndex(index) {
			this.tabBarIndex = index;
		},
		changeTabBarShow(showStatus) {
			this.tabBarShow = showStatus;
		},
		getThemeArr() {
			return themeArr;
		},
		/**
		 * 获取颜色值
		 * isLinear 是否线性
		 */
		getThemeColor(colorName, type = '', themeMode) {
			let currTheme = themeMode;

			if (themeMode === 'auto') {
				currTheme = this.theme;
			}
			if (themeMode === 'inverse') {
				if (this.theme === 'light') {
					currTheme = 'dark';
				}
				if (this.theme === 'dark') {
					currTheme = 'light';
				}
			}

			function getColor(name) {
				const nameArr = name.split('-');
				return nameArr.length === 2
					? themeArr[currTheme][name]
					: themeArr[currTheme][name] || themeArr[currTheme][name + '-5'];
			}

			// Example：45deg,red-400,blue-400
			if (type === 'linear') {
				// 角度，颜色1，颜色2
				const linearArr = colorName.split(',');
				const color1 = getColor(linearArr[1]);
				const color2 = getColor(linearArr[2]);
				return `linear-gradient(${linearArr[0]},${color1},${color2})`;
			}

			if (colorName) {
				switch (colorName) {
					case 'primary':
						colorName = 'blue-5';
						break;
					case 'success':
						colorName = 'green-5';
						break;
					case 'warning':
						colorName = 'orange-5';
						break;
					case 'error':
						colorName = 'red-5';
						break;
					default:
						break;
				}
				return getColor(colorName);
			}
		},
		changeTheme(theme) {
			console.log('切换主题为：', theme);
			uni.setStorageSync('ac_theme', theme);
			this.theme = theme;
		},
	},
});
