# 图片 Image

## 说明

图片。

## 示例效果

<div class="uniapp-demo-box">

<<< ./src/pages/showdata/avatar.vue
<webview url="https://tmui.design/h5/#/pages/showdata/avatar"></webview>

</div>

## 示例

```vue
<ac-image></ac-image>
```

## 参数 Props

| 属性名 | 说明 | 类型 | 默认值 | 可选值 |
| --- | --- | --- | --- | --- |
| src | 图片地址 | string | - | - |
| mode | 图片缩放模式 | string | - | - |
| alt | 替代文本 | string | - | - |
| lazy-load | 是否开启懒加载 | boolean | false | - |
| show-error | 是否展示图片加载失败提示 |  |  |  |
| show-loading | 是否展示图片加载中提示 |  |  |  |
| error-icon | 失败时提示的图标名称或图片链接，等同于 Icon 组件的 name 属性 |  |  |  |
| error-text |  |  |  |  |
| loading-icon | 加载时提示的图标名称或图片链接，等同于 Icon 组件的 name 属性 |  |  |  |
| loading-text |  |  |  |  |
| icon-size | 加载图标和失败图标的大小 |  |  |  |

## 事件 Event

| 事件名 | 描述 | 参数 | 返回值 |
| --- | --- | --- | --- |
| click | - | - | - |
| load | 图片加载完毕时触发 |  | - |
| error | 图片加载失败时触发 | - | - |

##  插槽 slot

| 插槽名称 | 描述 |
| -------- | ---- |
| default  | -    |

## 注意事项

无
