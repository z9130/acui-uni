# 插件说明 Plugins



## 必备插件

### Unocss

### 介绍

原子化css插件，可减轻项目体积，可通过配置集成各种图标库。

## 安装

``` shell
pnpm add unocss -D
pnpm add unocss-preset-weapp -D
```



### pinia-plugin-persist-uni

#### 介绍

pinia 持久化存储插件。

#### 安装

``` shell
// pinia 安装版本超过2.0.34可能会报错。
pnpm add pinia@2.0.34
pnpm add pinia-plugin-persist-uni
```



### 可选插件