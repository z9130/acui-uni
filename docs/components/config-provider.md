# 全局配置 Config Provider


## 介绍

123

## 示例效果

<div class="uniapp-demo-box">

<<< ./src/pages/showdata/avatar.vue
<webview url="https://tmui.design/h5/#/pages/showdata/avatar"></webview>

</div>

## 参数 Props

| 属性相关   | 说明                                             | 类型           | 默认值  | 可选值 |
| ---------- | ------------------------------------------------ | -------------- | ------- | ------ |
| theme      | 主题风格，设置为 `dark` 来开启深色模式，全局生效 | `dark`/`light` | `light` | `dark` |
| theme-vars | 自定义主题变量，局部生效                         | object         | -       | -      |

## 插槽 Slot

| 插槽名称 | 描述 |
| -------- | ---- |
| default  | -    |

## 注意事项

无