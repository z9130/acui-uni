import { useAcPiniaStore } from '@/uni_modules/ac-ui/store/acpinia';


export const pub_props = {
	theme: {
		type: String,
		default: 'auto', // dark light inverse(反色，浅色模式下为深色；深色模式下为浅色)
	},
};

//自定义样式计算属性。
export function computedMarginPadding(props) {
	if (props instanceof Array) {
		props.forEach((prop, index) => {
			if (typeof prop == 'number') {
				props[index] = prop + 'rpx';
			}
			if (typeof prop == 'string') {
				props[index] = prop;
			}
		});
		switch (props.length) {
			case 2:
				return `${props[0]} ${props[1]}`;
			case 4:
				return `${props[0]} ${props[1]} ${props[2]} ${props[3]}`;
			default:
				return '0rpx 0rpx 0rpx 0rpx';
		}
	} else {
		return '0rpx 0rpx 0rpx 0rpx';
	}
}

/**
 * @param type 'linear'
 */
export function computedColor(color, type = '', themeMode = 'auto') {
	// console.log(color, type, themeMode);
	const store = useAcPiniaStore();
	if (!color) {
		return;
	}
	if (color.indexOf('#') > -1) {
		return color;
	}
	
	if (color === 'transparent' || color === 'none') {
		return 'transparent';
	}
	return store.getThemeColor(color, type, themeMode);
}

export function bgColor(color, type = '', themeMode = 'auto') {
	return { 'background-color': computedColor(color, type, themeMode) };
}

export function fontColor(color, type = '', themeMode = 'auto') {
	return { 'color:': computedColor(color, type, themeMode) };
}

export function computedGray(color, type = '', themeMode = 'auto') {
	const store = useAcPiniaStore();
	return store.isGrayPage;
}
// 2rpx 2rpx 10rpx #C0C0C0
