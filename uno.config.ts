import type { Preset, SourceCodeTransformer } from 'unocss'
import {
  defineConfig,
  presetAttributify,
  presetIcons,
  presetUno,
  transformerDirectives,
  transformerVariantGroup,
} from 'unocss'

import presetWeapp from 'unocss-preset-weapp';
import { transformerAttributify, transformerClass } from 'unocss-preset-weapp/transformer';

// import presetWeapp from 'unocss-preset-weapp';

const isApplet = process.env?.UNI_PLATFORM?.startsWith('mp-') ?? false
const presets: Preset[] = []
const transformers: SourceCodeTransformer[] = []

// if (isApplet) {
//   /**
//    * UnoCSS Applet
//    * @see https://github.com/unocss-applet/unocss-applet
//    */
//   presets.push(presetApplet())
//   // 如果需要使用 rem 转 rpx 单位，需要启用此插件
//   transformers.push(transformerAttributify())
//   transformers.push(transformerApplet())
// }
// else {
//   presets.push(presetUno())
//   presets.push(presetAttributify())
// }

// presets.push(presetRemRpx()) 

const prefix = ``;
const transformRules = {
  '.': '-d111-',
  '/': '-s111-',
  ':': '-c111-',
  '%': '-p111-',
  '!': '-e111-',
  '#': '-w111-',
  '(': '-b111l-',
  ')': '-b111r-',
  '[': '-f111l-',
  ']': '-f111r-',
  $: '-r111-',
  ',': '-r222-',
};

export default defineConfig({
  rules: [
    [/^w-(\d+)$/, ([, d]) => ({ "width": `${Number(d)}rpx` })],
    [/^h-(\d+)$/, ([, d]) => ({ "height": `${Number(d)}rpx` })],
    [/^m-(\d+)$/, ([, d]) => ({ "margin": `${Number(d)}rpx` })],
    [/^mx-(\d+)$/, ([, d]) => ({ "margin-left": `${Number(d)}rpx`, "margin-right": `${Number(d)}rpx` })],
    [/^my-(\d+)$/, ([, d]) => ({ "margin-top": `${Number(d)}rpx`, "margin-bottom": `${Number(d)}rpx` })],
    [/^mt-(\d+)$/, ([, d]) => ({ 'margin-top': `${Number(d)}rpx` })],
    [/^mb-(\d+)$/, ([, d]) => ({ 'margin-bottom': `${Number(d)}rpx` })],
    [/^ml-(\d+)$/, ([, d]) => ({ 'margin-left': `${Number(d)}rpx` })],
    [/^mr-(\d+)$/, ([, d]) => ({ 'margin-right': `${Number(d)}rpx` })],
    [/^p-(\d+)$/, ([, d]) => ({ padding: `${Number(d)}rpx` })],
    [/^px-(\d+)$/, ([, d]) => ({ "padding-left": `${Number(d)}rpx`, "padding-right": `${Number(d)}rpx` })],
    [/^py-(\d+)$/, ([, d]) => ({ "padding-top": `${Number(d)}rpx`, "padding-bottom": `${Number(d)}rpx` })],
    [/^pt-(\d+)$/, ([, d]) => ({ 'padding-top': `${Number(d)}rpx` })],
    [/^pb-(\d+)$/, ([, d]) => ({ 'padding-bottom': `${Number(d)}rpx` })],
    [/^pl-(\d+)$/, ([, d]) => ({ 'padding-left': `${Number(d)}rpx` })],
    [/^pr-(\d+)$/, ([, d]) => ({ 'padding-right': `${Number(d)}rpx` })],
  ],
  presets: [
    // @ts-ignore
    presetWeapp({
      nonValuedAttribute: true,
      prefix: prefix,
      whRpx: true,
      transform: true,
      platform: 'uniapp',
      transformRules,
    }),
    // 由 Iconify 提供支持的纯 CSS 图标解决方案
    presetIcons({
      scale: 1.2,
      warn: true,
    }),
    ...presets,
  ],
  /**
   * 自定义快捷语句
   * @see https://github.com/unocss/unocss#shortcuts
   */
  shortcuts: [
    {
      'flex-row': 'flex flex-row',
      'flex-col': 'flex flex-col',
      'flex-center': 'flex justify-center items-center',
    },
    ['btn', 'px-4 py-1 rounded inline-block bg-teal-600 text-white cursor-pointer hover:bg-teal-700 disabled:cursor-default disabled:bg-gray-600 disabled:opacity-50'],
    ['icon-btn', 'text-[0.9em] inline-block cursor-pointer select-none opacity-75 transition duration-200 ease-in-out hover:opacity-100 hover:text-teal-600 !outline-none'],
  ],
  transformers: [
    transformerDirectives(), // 启用 @apply 功能
    transformerVariantGroup(), // 启用 () 分组功能
    ...transformers,
  ],
})
