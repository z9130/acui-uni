/**
 * 函数 - 跳转
 */
export default {
	// uni.$ac.encodeUrl()
	encodeUrl: function (obj = {}) {
		return encodeURIComponent(JSON.stringify(obj));
	},
	// uni.$ac.decodeUrl()
	decodeUrl: function (obj = {}) {
		return JSON.parse(decodeURIComponent(obj));
	},
};
