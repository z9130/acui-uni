import { defineConfig } from 'vitepress'
import { chineseSearchOptimize, pagefindPlugin } from 'vitepress-plugin-pagefind'
import { SearchPlugin } from "vitepress-plugin-search";
import flexSearchIndexOptions from "flexsearch";

import navConfig from "./navConfig";
import sidebarConfig from "./sidebarConfig";

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "ACUI",
  description: "基于uni-app开发的一款轻量、全面可靠的跨平台移动端组件库。",
  base: "/ac-ui/",// gitee域名配置
  lang: 'zh-CN',
  vite: {
    plugins: [

      SearchPlugin({
        ...flexSearchIndexOptions,
        previewLength: 100, //搜索结果预览长度
        buttonLabel: "搜索",
        placeholder: "情输入关键词",
        tokenize: 'full',
      }),
      // 参考：https://www.npmjs.com/package/vitepress-plugin-pagefind
      // 文档：https://juejin.cn/post/7217381222025068600?from=search-suggest
      // pagefindPlugin({
      //   forceLanguage: 'zh-cn',
      //   btnPlaceholder: '键入关键记事',
      //   placeholder: '搜索文档',
      //   emptyText: '空空如也',
      //   heading: '共: {{searchResult}} 条结果',
      //   excludeSelector: ['img', 'a.header-anchor', 'div'],
      //   customSearchQuery(input) {
      //     // 将搜索的每个中文单字两侧加上空格
      //     return input.replace(/[\u4e00-\u9fa5]/g, ' $& ')
      //       .replace(/\s+/g, ' ')
      //       .trim();
      //   }
      // })
    ],
  },
  themeConfig: {
    nav: navConfig,
    sidebar: sidebarConfig,
    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ],
    docFooter: {
      prev: 'Pagina prior',
      next: 'Proxima pagina'
    },
    editLink: {
      pattern: 'https://github.com/vuejs/vitepress/edit/main/docs/:path',
      text: '编辑此页'
    },
    outlineTitle: '本页目录', // On this Page
    lastUpdatedText: '最近更新时间',
    search: {
      provider: 'local'
    },
    footer: {
      message: 'Released under the MIT License.',
      copyright: 'Copyright © 2019-present Evan You'
    }
  },
})
