#  数据Mock

## 说明

数据mock，提供对指定接口的数据模拟功能，但涉及代码侵入，个人看法非必要不建议使用。可借助第三方工具搭建 `mock` 服务，如 `ApiPost` 或 `ApiFox` 都具有接口 `mock` 的功能。

## 快速上手



### 安装

```shell
pnpm add vite-plugin-mock -D
```



### 引入

```typescript
// vite.config.ts

import { viteMockServe } from 'vite-plugin-mock'

export default defineConfig((mode: ConfigEnv) => {
  return {
      plugins: [
          viteMockServe({
            mockPath: './mock/', // 设置接口mock存放位置
            //@ts-ignore
            supportTs: true,//是否读取ts文件模块
            logger: true,//是否在控制台显示请求日志
            localEnabled: true,//设置是否启用本地mock文件
            prodEnabled: true//设置打包是否启用mock功能
          }),
      ]
  }
})
```



### 配置

项目根目录下新建  `mock` 文件夹（与 `vite.config.ts` 文件同级），



## 常见问题

