# 文本 Text

> 文本

## 示例
```vue
<ac-text font-size="xxl" color="gray-10" text="标题"></ac-text>
<ac-text font-size="lg" color="gray-9" text="副标题"></ac-text>
<ac-text font-size="md" color="gray-8" text="正文"></ac-text>
<ac-text :font-size="32" color="gray-8" text="正文"></ac-text>

```
## 参数
| 属性名 | 说明 | 类型 | 默认值 | 可选值 |
| --- | --- | --- | --- | --- |
| text | 文本内容 | string | - | - |

| 样式名 | 说明 | 类型 | 默认值 | 可选值 |
| --- | --- | --- | --- | --- |
| font-size | 文本大小 | string / number | 'md' / 32 | 'xxs','xs','sm','md','lg','xl','xxl' / 任意数字 |
| color | 文字颜色 | string | 'grey-10' | [主题颜色](https://www.yuque.com/acui/docs/fwshtn) |
| _class | - | string | - | - |
| _style | - | string | - | - |

## 事件
| 事件名 | 描述 | 参数 | 返回值 |
| --- | --- | --- | --- |
| click | - | - | - |
| longpress | - | - | - |

## 注意事项
无
